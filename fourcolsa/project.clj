(defproject fourcolsa "0.1.0-SNAPSHOT"
  :description "First attempt solution 4 col problem"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/tools.cli "0.3.1"]
                 [com.taoensso/timbre "3.2.1"]
                 [incanter "1.5.5"]
                 [org.clojure/core.logic "0.8.8"]
                 [org.clojure/algo.monads "0.1.5"]
                 [instaparse "1.3.2"]
                 [prismatic/plumbing "0.3.3"]
                 [prismatic/schema "0.2.4"]]

  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]]

  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.4"]
                                  [org.clojure/tools.trace "0.7.8"]
                                  [midje "1.6.3"
                                   :exclusions [joda-time
                                                commons-codec]]]}})
