(ns fourcolsa.core
  "First solution: many obvious optimizations passed on"
  (:require [taoensso.timbre :as log]
            [plumbing.core :as plumbing]
            [schema.core :as s]
            [midje.sweet :refer :all]))

(background (around :facts (s/with-fn-validation ?form)))

(def map1 {1 [2 3] 2 [1 4] 3 [1 4] 4 [2 3]})

(def SColMap {Number #{Number}})
(def SMap {Number [Number]})

(s/defn setup-coloring :- SColMap
  "Setup initial domains for all nodes.  Initially all can have all
   colors."
  [adjacency-map :- SMap]
  (let [nodes   (keys adjacency-map)
        colors  #{1 2 3 4}]
    (reduce (fn [m n] (assoc m n colors))
            {}
            nodes)))

(facts "setup coloring"
  (setup-coloring map1) => {1 #{1 2 3 4}
                            2 #{1 2 3 4}
                            3 #{1 2 3 4}
                            4 #{1 2 3 4}})

(s/defn propagate-constraint :- SColMap
  "Given that vertex has color color, reduce the domains in colmap to
   only have colors consistent with this."
  [[vertex color]
   color-map :- SColMap
   adjacency-map :- SMap]
  (let [nbds (adjacency-map vertex)]
    (assoc (reduce (fn remove-color-nbd [acc nbd]
                     (update-in acc
                                [nbd]
                                (fn remove-color-set [s] (disj s color))))
                   color-map
                   nbds)
      vertex
      #{color})))

(facts "propagate constraint"
  (propagate-constraint [1 1] (setup-coloring map1) map1)
  => {4 #{1 2 3 4}, 3 #{2 3 4}, 2 #{2 3 4}, 1 #{1}})

(s/defn adjacent-same-col :- (s/either (s/eq true) (s/eq false) (s/eq nil))
  [color-map :- SColMap
   adjacency-map :- SMap]
  (some identity (for [[vertex nbds] adjacency-map
              nbd           nbds
              col1          [(color-map vertex)]
              col2          [(color-map nbd)]
              :when (and (= 1 (count col1))
                         (= 1 (count col2)))]
          (= col1 col2))))

(facts "adjacent-same-col"
  (adjacent-same-col {1 #{1} 2 #{1}} {1 [2] 2 [1]}) => true
  (adjacent-same-col {1 #{1} 2 #{2}} {1 [2] 2 [1]}) => nil
  (adjacent-same-col {1 #{1} 2 #{2 1}} {1 [2] 2 [1]}) => nil)

(s/defn failed? :- (s/either (s/eq true) (s/eq nil) (s/eq false))
  [color-map :- SColMap
   adjacency-map :- SMap]
  (or (some empty? (vals color-map))
      (adjacent-same-col color-map adjacency-map)))

(facts "failed?"
  (failed? {1 #{}} map1) => true
  (failed? {1 #{1}} map1) => nil
  (failed? {1 #{1 2} 2 #{}} map1) => true
  (failed? {1 #{1} 2 #{2}} map1) => nil
  (failed? {1 #{1} 2 #{1}} map1) => true
  (failed? {1 #{1} 2 #{2}} map1) => nil)

(s/defn finished? :- (s/either (s/eq true) (s/eq false))
  [color-map :- SColMap]
  (every? (fn [s] (= 1 (count s))) (vals color-map)))

(facts "finished?"
  (finished? {1 #{}}) => false
  (finished? {1 #{1}}) => true
  (finished? {1 #{1} 2 #{2}}) => true
  (finished? {1 #{1} 2 #{2 3}}) => false)

(s/defn next-choices :- [(s/one Number "vertex") (s/one #{Number} "cols")]
  "Returns a vertex and possible colors to choose from next"
  [color-map :- SColMap]
  (first (filter (fn [[_ cols]] (< 1 (count cols))) color-map)))

(facts "next-choices"
  (next-choices {1 #{1 2}}) => [1 #{1 2}]
  (next-choices {1 #{1} 2 #{2 3}}) => [2 #{2 3}])

(s/defn get-coloring ;; :- [SColMap] doesn't work b/c of nestings and nils
  ([adjacency-map :- SMap]
     (remove nil?
             (flatten
              (get-coloring adjacency-map
                            (setup-coloring adjacency-map)))))
  ([adjacency-map :- SMap
    color-map :- SColMap]
     (cond
      (failed? color-map adjacency-map)
      nil

      (finished? color-map)
      color-map

      :default
      (let [[vertex cols] (next-choices color-map)]
        (for [col cols]
          (get-coloring adjacency-map
                     (propagate-constraint [vertex col]
                                           color-map
                                           adjacency-map)))))))

(facts "get-coloring"
  (first (get-coloring map1))
  => {4 #{1}, 3 #{2}, 2 #{2}, 1 #{1}}

  (second (get-coloring map1))
  => {4 #{1}, 3 #{2}, 2 #{2}, 1 #{3}})

;; full graph on 5 vertices; cannot be colored in 4 colors (is not
;; planar).
(get-coloring {1 [2 3 4 5]
               2 [1 3 4 5]
               3 [1 2 4 5]
               4 [1 2 3 5]
               5 [1 2 3 4]})
