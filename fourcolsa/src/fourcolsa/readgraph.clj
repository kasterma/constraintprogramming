(ns fourcolsa.readgraph
  (:require [taoensso.timbre :as log]
            [plumbing.core :as plumbing]
            [schema.core :as s]
            [midje.sweet :refer :all]))

(defn readgraph [filename]
  (let [lines    (clojure.string/split-lines (slurp filename))
        vertices (range 1 (inc (read-string (first lines))))
        nbdlists (map (comp (partial mapv read-string)
                         (fn [s] (clojure.string/split s #" ")))
                      (rest lines))
        dat      (map vector vertices nbdlists)]
    (into {}  dat)))
