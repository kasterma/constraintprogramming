(ns queens.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.algo.monads :as monads]
            [instaparse.core :as insta]
            [clojure.core.typed :as typed])
  (:use midje.sweet)
  (:gen-class))

;;; # 8 Queens in Plain Clojure

(def rows (range 1 9))
(def cols (range 1 9))
(def empty-board (combinat/cartesian-product rows cols))

(defn up-diag
  [[x y]]
  (- x y))

(fact
  (up-diag [1 2]) => -1)

(defn down-diag
  [[x y]]
  (+ x y))

(fact
  (down-diag [1 2]) => 3)

(defn disallowed
  [[px py :as placed] [ox oy :as other]]
  (or (= px ox)
      (= py oy)
      (= (up-diag placed) (up-diag other))
      (= (down-diag placed) (down-diag other))))

(facts
  (disallowed [1 2] [1 3]) => true
  (disallowed [1 2] [2 2]) => true
  (disallowed [1 2] [2 3]) => true
  (disallowed [1 2] [2 6]) => false
  (disallowed [1 2] [7 7]) => false)

(defn propagate-constrait
  [available disallow-fn]
  (remove disallow-fn available))

(facts
  (propagate-constrait [[1 1] [1 2] [1 3] [1 4]] (partial disallowed [2 2]))
  =>
  [[1 4]]

  (propagate-constrait empty-board (partial disallowed [3 3]))
  =>
  '((1 2) (1 4) (1 6) (1 7) (1 8)
    (2 1) (2 5) (2 6) (2 7) (2 8)
    (4 1) (4 5) (4 6) (4 7) (4 8)
    (5 2) (5 4) (5 6) (5 7) (5 8)
    (6 1) (6 2) (6 4) (6 5) (6 7) (6 8)
    (7 1) (7 2) (7 4) (7 5) (7 6) (7 8)
    (8 1) (8 2) (8 4) (8 5) (8 6) (8 7)))

(defn solve
  "given the available coordinates, find a placement for
   number many queens.  Returns the placement or false if there
   is no such placement."
  [available number]

  (cond
   (zero? number)
   []

   (empty? available)
   false

   :default
   (loop [pos   (first available)
          rem   (rest available)]
     (if-let   [sol    (solve (propagate-constrait
                               available (partial disallowed pos))
                              (dec number))]
       (conj sol pos)
       (if (empty? rem)
         false
         (recur (first rem) (rest rem)))))))

(facts
  (solve [[1 1]] 1) => [[1 1]]
  (solve [[1 1] [3 4]] 2) => [[3 4] [1 1]]
  (solve [[1 1] [3 4] [3 3]] 2) => [[3 4] [1 1]]
  (solve [[1 1] [3 4] [3 3]] 3) => false
  (solve empty-board 1) => ['(1 1)]
  (solve empty-board 2) => ['(2 3) '(1 1)]
  (solve empty-board 3) => ['(3 5) '(2 3) '(1 1)]
  (solve empty-board 4) => ['(4 2) '(3 5) '(2 3) '(1 1)]
  (solve empty-board 5) => ['(5 4) '(4 2) '(3 5) '(2 3) '(1 1)]
  (solve empty-board 6) =>
  ['(7 4) '(5 8) '(4 2) '(3 5) '(2 3) '(1 1)]
  (solve empty-board 7) =>
  ['(8 7) '(7 4) '(5 8) '(4 2) '(3 5) '(2 3) '(1 1)]
  (solve empty-board 8) =>
  ['(8 4) '(7 2) '(6 7) '(5 3) '(4 6) '(3 8) '(2 5) '(1 1)]
  ;; (solve empty-board 9) => false
  ;; commented out because it takes a long time.
  )
