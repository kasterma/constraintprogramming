(ns queens.minmax
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :refer (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :refer (p profile)]
            [criterium.core :as criterium])
  (:use midje.sweet))

;; Simple implementation of min-max solution of 8 queens

;;; Boards are square of this size.
(def board-size 8)

;;; A placement consists of a list of n elements where the i-th
;;; element indicates the row the queen in the i-th column is in.

(defn initial-placement
  "Give a random initial placement"
  ([]
     (initial-placement board-size))
  ([size]
     (vec (repeatedly size #(rand-int size)))))

(defn violate [[c1 r1] [c2 r2]]
  (or (= c1 c2)
      (= r1 r2)
      (= (- r1 r2) (- c1 c2))
      (= (- r1 r2) (- c2 c1))))

(facts "violations correctly detected"
  (violate [..anything.. 1] [..anything_else.. 1]) => true
  (violate [1 1] [2 2]) => true
  (violate [1 1] [2 3]) => false
  (violate [1 2] [2 1]) => true
  (violate [1 ..anything..] [1 ..anything_else..]) => true)

(defn count-violations
  "count the number of violations queen in col idx is involved in"
  ([placement col row]
     (apply + (for [i2  (range (count placement))
                    :when (and (not= i2 col)
                               (violate [col row] [i2 (nth placement i2)]))]
                1)))
  ([placement col]
     (count-violations placement col (nth placement col))))

(facts "counting violations"
  (count-violations '(1 2 3) 1) => 2
  (count-violations '(1 2 3 4) 1) => 3
  (count-violations '(1 2 3 4) 3) => 3
  (count-violations '(1 2 2 4) 2) => 1
  (count-violations '(1 2 2 4) 2 3) => 3)

(defn step
  [placement moved]
  (let [r     (range (count placement))
        max   (p :max (apply max-key #(count-violations placement %) (remove #(= moved %) r)))
        min   (p :min (apply min-key #(count-violations placement max %) r))
        new   (assoc placement max min)
        vioct (p :vioct (apply + (map #(count-violations new %) r)))]
    {:new new :violations-count vioct :moved max}))

(criterium/with-progress-reporting
  (criterium/quick-bench (step [0 1 2 3] 0)
                         :verbose))

(facts "taking a single step"
  (step [0 1 2 3] 0) => {:moved 3 :new [0 1 2 2] :violations-count 8}
  (step [0 1 2] 0) => {:moved 2 :new [0 1 1] :violations-count 4})

(defn solve
  []
  (loop [{:keys [new violations-count moved] :or {violations-count 1} :as dat} {:new (initial-placement)}
         it-count 1]
    (println (pr-str dat))
    (if (or (> it-count 100) (zero? violations-count))
      (println "done")
      (recur (step new moved) (inc it-count)))))
