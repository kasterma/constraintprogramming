(defproject queens "0.1.0-SNAPSHOT"
  :description "different solutions to the 8-queens problem"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [org.clojure/tools.cli "0.2.2"]
                 [midje "1.5.0"]
                 [org.clojure/tools.trace "0.7.5"]
                 [com.taoensso/timbre "1.6.0"]
                 [incanter "1.4.1"]
                 [org.clojure/tools.cli "0.2.2"]
                 [org.clojure/core.logic "0.8.3"]
                 [criterium "0.4.2"]]
  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]])
